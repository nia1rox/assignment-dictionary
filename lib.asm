section .text
 
global string_length
global print_string
global print_err
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global exit
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    mov rdi, 0
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
    .loop:
    	cmp byte [rdi + rax], byte 0
    	jz .end
    	inc rax
    	jmp .loop
    .end:
    	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret
    
; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_err:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 2
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r9, 10
    mov rax, 0
    mov rcx, 0
    dec rsp
    mov [rsp], byte 0
    mov rax, rdi
    .to_dec:
  	mov rdx, 0
  	div r9
  	add dl, '0'
  	dec rsp
  	mov [rsp], dl
  	inc rcx
  	cmp rax, 0
  	jnz .to_dec
    .print:
    	mov rdi, rsp
    	inc rcx
    	push rcx
    	call print_string
    	pop rcx
    	add rsp, rcx
    	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov r9, 0
    mov rcx, 0
    .cmp:
    	mov r9b, byte [rdi + rcx]
    	cmp r9b, byte [rsi + rcx]
    	jne .end
    	cmp r9, 0
    	jz .success
    	inc rcx
    	jmp .cmp
    .success:
    	mov rax, 1
    	ret	
    .end:
    	mov rax, 0	
    	ret
    	

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, 0
    jz .end
    mov al, byte [rsp]
    pop rdi
    mov rdi, 0
    ret
    .end:
    	mov rax, 0
    	pop rdi
    	mov rdi, 0
    	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    dec rsi
    mov r9, rsi
    xor r10, r10
    .validate:
  	call read_char
  	cmp rax, 0x20
  	je .validate
  	cmp rax, 0x9
  	je .validate
  	cmp rax, 0xA
  	je .validate
    .next_char:
  	cmp rax, 0
  	jz .success
  	cmp rax, 0x20
  	je .success
  	cmp rax, 0x9
  	je .success
  	cmp rax, 0xA
  	je .success
  	cmp r10, r9
  	je .error
  	mov [r8 + r10], rax
  	inc r10
  	call read_char
  	jmp .next_char
    .success:
  	mov [r8 + r10], byte 0
  	mov rax, r8
  	mov rdx, r10
  	ret
    .error:
  	xor rax, rax
  	ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
    mov r9, 0
    mov r10, 10
    mov rdx, 0
    .read_num:
    	mov r9b, [rdi + rdx]
    	cmp r9b, '9'
    	ja .fault
    	cmp r9b, '0'
    	jb .fault
    	sub r9b, '0'
    	push rdx
    	mul r10
    	pop rdx
    	add rax, r9
    	inc rdx
    	jmp .read_num
    .fault:	
    	ret
    	
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov rax, 0
    cmp byte [rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    inc rax
    cmp rax, rdx
    ja .fault
    mov rcx, 0
    .cp_char:
    	mov r9b, byte [rdi + rcx]
    	mov byte [rsi + rcx], r9b
    	cmp r9b, 0
    	jz .success
    	inc rcx
    	jmp .cp_char
    .fault:
    	mov rax, 0
    	ret
    .success:
    	mov rax, rcx
    	ret
