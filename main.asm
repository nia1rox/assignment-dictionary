section .text

%define BUFFER_SIZE 256
%define ADDRESS_SPACE 8

%include "colon.inc"

extern print_string
extern print_err
extern print_newline
extern read_word
extern find_word
extern exit

global _start
section .rodata

input_msg: db 'Enter key', 0
key_not_found: db 'key not found', 0
input_err: db 'Too large key', 0

section .text

_start:
	mov rdi, input_msg
	call print_string
    	sub rsp, BUFFER_SIZE
    	mov rdi, rsp
    	mov rsi, BUFFER_SIZE 
    	call read_word
    	cmp rax, 0
    	jz .err
    	mov rdi, rax
    	mov rsi, last
    	call find_word 
    	cmp rax, 0
    	jz .not_find
    	mov rdi, rax
    	add rdi, ADDRESS_SPACE
    	inc rdi
    	call print_string
    	call exit
    	.not_find:
    		mov rdi, key_not_found
    		jmp .end
    	.err:
    		mov rdi, input_err	
	.end:
    		call print_err
    		call exit

