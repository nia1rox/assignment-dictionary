%define ADDRESS_SPACE 8

global find_word
extern string_equals

section .text

;Указатель на нуль-терминированную строку.
;Указатель на начало словаря.
;find_word пройдёт по всему словарю в поисках подходящего ключа.
;Если подходящее вхождение найдено, вернёт адрес начала вхождения в 
;словарь (не значения), иначе вернёт 0.
find_word:
	mov rax, 0
	.loop:
		cmp rsi, 0
		jz .not_found
		push rsi
		add rsi, ADDRESS_SPACE
		push rdi
		call string_equals
		pop rdi
		pop rsi
		jnz .found
		mov rsi, [rsi]
		jmp .loop
	.found:
		mov rax, rsi
	.not_found:
		ret	
