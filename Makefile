ASM=nasm
FLAGS=-f elf64 -o

build: dict.o lib.o main.o
	ld -o $@ $^
dict.o: dict.asm
	$(ASM) $(FLAGS) dict.o dict.asm

lib.o: lib.asm
	$(ASM) $(FLAGS) lib.o lib.asm

main.o: main.asm colon.inc words.inc
	$(ASM) $(FLAGS) main.o main.asm

clean:
	rm -f *.o build
